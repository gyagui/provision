This project provision a Ubuntu 14.04 machine running a Wordpress application behind a Nginx forward proxy.

Prerequisites:

* Git (to clone this repo)
* Ansible 2.1.x
* Vagrant 1.8.x
* Virtualbox 5.0

### Notes ###

Assure the IP of the static node in Vagrantfile is unique in the network.

### Contributors ###

* guilherme.takasawa@gmail.com